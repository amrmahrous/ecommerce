<?php

?>

<div class="form">
<form id="setting-form" action="/ecommerce/admin/setting/update" method="post">
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	
	<div class="row">
	<label for="Setting_site_name" class="required">Site Name <span class="required">*</span></label>
	<input size="60" maxlength="255" name="Setting[site_name]" id="Setting_site_name" type="text" value="<?php echo  $setting_data['site_name'] ; ?>">
	</div>

	<div class="row">
	<label for="Setting_site_desc" class="required">Site Description <span class="required">*</span></label>
	<textarea rows="6" cols="50" name="Setting[site_desc]" id="Setting_site_desc"><?php echo  $setting_data['site_desc'] ; ?></textarea>
	</div>

	<div class="row">
	<label for="Setting_min_shipment_items" class="required">Minimum items per shipment <span class="required">*</span></label>
	<input size="60" maxlength="255" name="Setting[min_shipment_items]" id="Setting_min_shipment_items" type="text" value="<?php echo  $setting_data['min_shipment_items'] ; ?>">
	</div>

	<div class="row buttons">
		<input type="submit" name="yt0" value="Save">	</div>

</form>

</div><!-- form -->