<?php
/* @var $this SettingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Settings',
);

$this->menu=array(
	array('label'=>'Edit Setting', 'url'=>array('update')),
);
?>

<h1>Settings</h1>
<table class="detail-view" id="yw0"><tbody>
<tr class="odd"><th>Site Name :</th><td><?php echo  $setting_data['site_name'] ; ?></td></tr>
<tr class="even"><th>Site Description : </th><td><?php echo  $setting_data['site_desc'] ; ?></td></tr>
<tr class="odd"><th>Minimum items per shipment : </th><td><?php echo  $setting_data['min_shipment_items'] ; ?></td></tr>
</tbody></table>
<?php 


/*
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 

*/
?>
