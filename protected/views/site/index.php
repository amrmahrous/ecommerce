<?php
/* @var $this SiteController */

$this->pageTitle= $this->setting()['site_name'];




?>
	<style type="text/css">

	body {
		font-family: arial, helvetica, serif;
	}
	
	#nav, #nav ul { /* all lists */
		padding: 0;
		margin: 0;
		list-style: none;
		float : left;
		width : 11em;
	}
	
	#nav li { /* all list items */
		position : relative;
		float : left;
		line-height : 1.25em;
		margin-bottom : -1px;
		width: 11em;
	}
	
	#nav li ul { /* second-level lists */
		position : absolute;
		left: -999em;
		margin-left : 11.05em;
		margin-top : -1.35em;
	}
	
	#nav li ul ul { /* third-and-above-level lists */
		left: -999em;
	}
	
	#nav li a {
		width: 11em;
		w\idth : 10em;
		display : block;
		color : black;
		font-weight : bold;
		text-decoration : none;
		background-color : white;
		border : 1px solid black;
		padding : 0 0.5em;
	}
	
	#nav li a:hover {
		color : white;
		background-color : black;
	}
	
	#nav li:hover ul ul, #nav li:hover ul ul ul, #nav li.sfhover ul ul, #nav li.sfhover ul ul ul {
		left: -999em;
	}
	
	#nav li:hover ul, #nav li li:hover ul, #nav li li li:hover ul, #nav li.sfhover ul, #nav li li.sfhover ul, #nav li li li.sfhover ul { /* lists nested under hovered list items */
		left: auto;
	}
	

</style>

<script type="text/javascript"><!--//--><![CDATA[//><!--

sfHover = function() {
	var sfEls = document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

//--><!]]></script>
<div class="span-5 last">
<ul id="nav">
<?php echo $categories;?>
	

</ul>
</div>
<div class="span-15">

<?php if($page=='index'){
	echo '<h1>Latest Products</h1>';
	}elseif($page=='category'){

	}?>
	<div class="span-15">
<?php
	foreach($products AS $product){ 
		if($product->qantity > 0){
		?>
		<div class="span-5">
			<h2><?php echo $product->name ?></h2>
			<span><?php echo $product->desc?></span><br/>
			<b>Price : $ <?php echo $product->price?></b><br/>
			<b>Available quantity :<?php echo $product->qantity?></b><br/>
			<form action="<?php echo Yii::app()->homeUrl.'site/cart/'; ?>" method="post">
			<div class="row">
			<label for="form_quantity" class="required">quantity <span class="required">*</span></label>
			<input name="form[quantity]" id="form_quantity" type="text" size='4' value='1'>
			<input name="form[product_id]" id="form_product_id" type="hidden" value="<?php echo $product->id ?>">
			<?php echo CHtml::submitButton('Add to cart'); ?>
			</div>
			</form>
		</div>
		<?
	}
	}
	?>
	</div>
</div>
