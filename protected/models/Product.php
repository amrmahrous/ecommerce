<?php

/**
 * This is the model class for table "tbl_product".
 *
 * The followings are the available columns in table 'tbl_product':
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $desc
 * @property integer $price
 * @property integer $qantity
 */
class Product extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_product';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('category_id, name, desc, price, qantity', 'required'),
            array('category_id, price, qantity', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, category_id, name, desc, price, qantity', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
        );
    }

    public function session_to_items($items = NULL) {
        $data = false;

        if ($items != NULL AND !empty($items) AND count($items) > 0) {
            $data['info']['total_price'] = 0;
            $data['info']['total_items'] = 0;
            $i = 0;
            foreach ($items AS $product) {
                $data['items'][$i]['product_id'] = key($product);
                $data['items'][$i]['order_quantity'] = $product[$data['items'][$i]['product_id']];
                $data['items'][$i]['product_id'] = key($product);
                $data['items'][$i]['product_info'] = $this->findByPk($data['items'][$i]['product_id']);
                $data['info']['total_price'] += ($data['items'][$i]['product_info']->price * $data['items'][$i]['order_quantity']);
                $data['info']['total_items'] += $data['items'][$i]['order_quantity'];
                $i++;
            }
        }

        return $data;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'category_id' => 'Category',
            'name' => 'Name',
            'desc' => 'Desc',
            'price' => 'Price',
            'qantity' => 'Qantity',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('desc', $this->desc, true);
        $criteria->compare('price', $this->price);
        $criteria->compare('qantity', $this->qantity);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Product the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
