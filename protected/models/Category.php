<?php

/**
 * This is the model class for table "tbl_category".
 *
 * The followings are the available columns in table 'tbl_category':
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $desc
 */
class Category extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_category';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('parent_id, desc', 'required'),
            array('parent_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, parent_id, name, desc', 'safe', 'on' => 'search'),
        );
    }

    public function category_tree($parent_id = null) {
        $categories = $this->findAllByAttributes(array('parent_id' => $parent_id));
        $data = false;
        if (count($categories)) {
            $i = 0;
            foreach ($categories AS $category) {
                $data[$i]['id'] = $category->id;
                $data[$i]['parent_id'] = $category->parent_id;
                $data[$i]['name'] = $category->name;
                $data[$i]['desc'] = $category->desc;

                $data[$i]['sub_categories'] = $this->category_tree($category->id);
                $i++;
            }
        }
        return $data;
    }

    public function category_html_helper($categories = false) {
        if ($categories == false) {
            $categories = $this->category_tree();
        };
        $data = '';

        if (count($categories) > 0) {
            $i = 0;
            foreach ($categories AS $category) {

                $data .= '<li><a href="' . Yii::app()->homeUrl . 'site/index/category_id/' . $category['id'] . '">' . $category['name'] . '</a>';
                if (!empty($category['sub_categories']) AND count($category['sub_categories']) > 0) {
                    $data .='<ul>';
                    $data .= $this->category_html_helper($category['sub_categories']);
                    $data .='</ul>';
                }
                $data .='</li>';
                $i++;
            }
        }
        return $data;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'parent_category' => array(self::BELONGS_TO, 'Category', 'parent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parent_id' => 'Parent',
            'name' => 'Name',
            'desc' => 'Desc',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('desc', $this->desc, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Category the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
