<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            
        );
    }
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','faq','error','contact','login','page','captcha'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('cart','shipment','logout'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array(),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public $data = array();

    public function actionIndex($category_id = null) {
        $category_html_helper = Category::model()->category_html_helper();
        $this->data['categories'] = $category_html_helper;
        $this->data['page'] = 'home';
        if ($category_id === null) {
            $this->data['products'] = Product::model()->findAll(array("limit" => "2", "order" => "id DESC",));
        } else {
            $this->data['page'] = 'category';
            $category_id = (int) $category_id;
            $this->data['products'] = Product::model()->findAllByAttributes(array('category_id' => $category_id));
        }
        $this->render('index', $this->data);
    }

    public function actionFaq() {
        $faq = new Faq;
        $this->data['faq'] = $faq->findAll();
        $this->render('faq', $this->data);
    }

    public function actionCart() {

        $cart = Yii::app()->user->getState('cart');
        $cart = unserialize($cart);

        if (isset($_POST['form'])) {

            $cart[] = array($_POST['form']['product_id'] => $_POST['form']['quantity']);
        }
        $cart = serialize($cart);

        Yii::app()->user->setState('cart', $cart);

        $category_html_helper = Category::model()->category_html_helper();
        $this->data['categories'] = $category_html_helper;

        $this->data['cart_products'] = Product::model()->session_to_items(unserialize($cart));
        $this->render('cart', $this->data);
    }

    public function actionShipment() {
        $cart = Yii::app()->user->getState('cart');
        if (isset($_POST['Shipment_form'])) {

            if ($_POST['Shipment_form']['action'] == 'cancel') {
                Yii::app()->user->setState('cart', NULL);
                $this->redirect(Yii::app()->homeUrl . 'site/cart/');
            }
            $category_html_helper = Category::model()->category_html_helper();
            $this->data['categories'] = $category_html_helper;
            if ($_POST['Shipment_form']['action'] == 'ship') {
                $cart = Yii::app()->user->getState('cart');
                $cart = unserialize($cart);
                $this->data['cart_products'] = Product::model()->session_to_items($cart);
                $site_setting = $this->setting();
                if ($this->data['cart_products']['info']['total_items'] < $site_setting['min_shipment_items']) {
                    Yii::app()->user->setFlash('error', "Minimum items in your cart below " . $site_setting['min_shipment_items']);
                } else {

                    $i = 0;


                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $shipment = new Shipment;
                        $shipment_data['products'] = serialize($cart);
                        $user = User::model()->findByAttributes(array('username' => Yii::app()->user->id) ); 
                        $shipment_data['user_id'] = $user->id;
                        $shipment_data['price'] = $this->data['cart_products']['info']['total_price'];
                        $shipment_data['order_time'] = date('Y-m-d H:i:s');
                        $shipment_data['status'] = '1';
                        $shipment_data['address'] = 'test';
                        $shipment_data['user_note'] = 'test';
                        //print_r($shipment_data);die;
                        $shipment->attributes = $shipment_data;
                        $shipment->save();
                        foreach ($cart AS $item) {
                            $data1[$i]['product_id'] = key($item);
                            $data1[$i]['qantity'] = $item[$data1[$i]['product_id']];
                            $product = Product::model()->findByPk($data1[$i]['product_id']);
                            $qantity = $product->qantity - $data1[$i]['qantity'];
                            Product::model()->updateByPk($data1[$i]['product_id'], array('qantity' => $qantity));
                            $i++;
                        }

                        $transaction->commit();
                        Yii::app()->user->setState('cart', NULL);
                        Yii::app()->user->setFlash('success', "We got your order and will contact you soon");
                        $this->redirect(Yii::app()->homeUrl);
                    } catch (Exception $e) {
                        $transaction->rollBack();
                        Yii::app()->user->setFlash('error', "{$e->getMessage()}");
                        $this->refresh();
                    }
                }
            }
        }

        $this->redirect(Yii::app()->homeUrl);
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}