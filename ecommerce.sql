-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 29, 2014 at 04:43 PM
-- Server version: 5.5.34-0ubuntu0.13.04.1
-- PHP Version: 5.4.9-4ubuntu2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `parent_id`, `name`, `desc`) VALUES
(1, 3, 'asfas', 'fasfasfsa'),
(3, NULL, 'test11', 'test5'),
(4, 1, 'lcd / led', 'lcd / led monitors and tvs'),
(5, 4, 'test cat 4', ''),
(6, 5, 'test cat 6', ''),
(7, NULL, 'desktop pcs', 'desktop computers'),
(8, NULL, 'mobile phones', 'mobiles'),
(9, NULL, 'laptop pcs', 'laptop computers'),
(10, NULL, 'demo cat1', ''),
(11, NULL, 'demo cat3', ''),
(12, NULL, 'demo cat3', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

DROP TABLE IF EXISTS `tbl_faq`;
CREATE TABLE `tbl_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`id`, `question`, `answer`) VALUES
(1, 'how users passwords are secured ?', 'the user passwords field in the database are secured using sha1 hash'),
(2, 'how to create a new admin', 'unfortunately not available in this beta  , we use username ''admin'' as admin user ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `price` int(11) NOT NULL,
  `qantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `category_id`, `name`, `desc`, `price`, `qantity`) VALUES
(1, 1, 'test', 'test', 49, 3123820),
(2, 3, 'test33', 'test pr', 30, 30);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

DROP TABLE IF EXISTS `tbl_setting`;
CREATE TABLE `tbl_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `key`, `value`) VALUES
(1, 'site_name', 'eCommerce sample demo1'),
(2, 'site_desc', 'description3'),
(3, 'min_shipment_items', '50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipment`
--

DROP TABLE IF EXISTS `tbl_shipment`;
CREATE TABLE `tbl_shipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `products` text NOT NULL,
  `price` int(11) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1',
  `order_time` datetime NOT NULL,
  `address` text NOT NULL,
  `user_note` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_shipment`
--

INSERT INTO `tbl_shipment` (`id`, `user_id`, `products`, `price`, `status`, `order_time`, `address`, `user_note`) VALUES
(1, '2', 'test', 44, 2, '0000-00-00 00:00:00', 'test address', 'test user note'),
(2, '2', 'test', 44, 1, '0000-00-00 00:00:00', 'test address', 'test user note'),
(3, '2', 'test', 44, 3, '2014-01-29 00:32:59', 'test address', 'test user note'),
(4, 'admin', 'a:1:{i:0;a:1:{i:1;s:2:"80";}}', 3920, 1, '2014-01-29 16:40:09', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipment_status`
--

DROP TABLE IF EXISTS `tbl_shipment_status`;
CREATE TABLE `tbl_shipment_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_shipment_status`
--

INSERT INTO `tbl_shipment_status` (`id`, `name`) VALUES
(1, 'pending payment'),
(2, 'pending delivery'),
(3, 'delivered  ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `email`) VALUES
(1, 'admin', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test1@example.com'),
(2, 'test2', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test2@example.com'),
(3, 'test3', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test3@example.com'),
(4, 'test4', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test4@example.com'),
(5, 'test5', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test5@example.com'),
(6, 'test6', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test6@example.com'),
(7, 'test7', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test7@example.com'),
(8, 'test8', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test8@example.com'),
(9, 'test9', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test9@example.com'),
(10, 'test10', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test10@example.com'),
(11, 'test11', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test11@example.com'),
(12, 'test12', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test12@example.com'),
(13, 'test13', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test13@example.com'),
(14, 'test14', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test14@example.com'),
(15, 'test15', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test15@example.com'),
(16, 'test16', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test16@example.com'),
(17, 'test17', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test17@example.com'),
(18, 'test18', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test18@example.com'),
(19, 'test19', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test19@example.com'),
(20, 'test20', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test20@example.com'),
(21, 'test21', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test21@example.com'),
(22, 'admin1', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'test@test.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
